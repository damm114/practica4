
package practica4;

/**
 *
 * @author damm113
 */
public class Perro {
    private String nombre;
    private String raza;
    
     
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void ladra() {
        System.out.println("Guau! guau! guau!");
    }

}
