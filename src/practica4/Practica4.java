/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica4;

/**
 *
 * @author damm114
 */
public class Practica4 {
    public static void main(String[] args){
        
        Perro perro1=new Perro();
        Perro perro2=new Perro();
        Perro perro3=new Perro();
        
        System.out.println("El perro 1:");
        perro1.ladra();
        System.out.println("El perro 2:");
        perro2.ladra();
        System.out.println("Ponemos el nombre al perro 1");
        perro1.setNombre("Pipo");
        System.out.println("El nombre del perro 1 es "+perro1.getNombre());
        System.out.println("El mejor perro "+perro1.getNombre());
        
        System.out.println(perro1.getNombre()+" tiene 4 años");
        
    }
}

